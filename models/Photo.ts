import {Entity} from './Entity'

export interface Photo extends Entity {
  title?: string
  url: Url
  type: PhotoType
}

export type Url = {
  original: string,
  fullHd: string,
  thumbnail: string
}

export enum PhotoType {
   BLACKANDWHITE = 'black-and-white',
   COLOR = 'color'
}