import {ObjectId} from 'bson'

export type Id = string | ObjectId

export interface WithTimestamp {
  createdAt: Date
  updatedAt: Date
}

export interface Entity extends WithTimestamp {
  _id: Id
}

export type OptionalTimestamps<T extends WithTimestamp> = Omit<T, 'createdAt' | 'updatedAt'> & {
  createdAt?: Date,
  updatedAt?: Date
}