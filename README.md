#Photo Gallery

This is a basic implementation of web gallery for desktop, that is deployed to [Heroku](https://heroku.com) via docker container, using MongoDB and the images are uploaded to [Amazon-S3](https://aws.amazon.com/s3/?did=ft_card&trk=ft_card), the site is live at -> [my-gallery](https://err-photo-gallery.herokuapp.com/)

##Installation

* Make sure you have at least node 15 and relevant npm + docker installed
* `git clone <project>` to clone project
* `npm i` for node modules

##Development:

* `docker-compose up -d` to run local MongoDB

###Run UI
`npm run start:ui`

###Run API
`npm run start:api`

##Stack
* Typescript
* NodeJS 15 (Express)
* ReactJS
* MongoDB
* Docker