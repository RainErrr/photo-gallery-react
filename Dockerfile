FROM node:15-alpine as build

WORKDIR /app

COPY package*.json ./
RUN npm i npm@7.13.0
RUN npm ci

COPY models/ ./models
COPY api/ ./api
COPY ui/ ./ui

COPY webpack.config.js tsconfig*.json jest.config.js custom.d.ts ./

RUN npm run build:ui
RUN npm run build:api


FROM node:15-alpine as final

RUN adduser -S user

WORKDIR /app

COPY package*.json ./
RUN npm ci --production

COPY --from=build /app/build .

USER user

CMD node ./api/src

ENV PORT=3000
EXPOSE $PORT