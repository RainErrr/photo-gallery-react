import {Photo, PhotoType} from 'models/Photo'
import api from '../Api'

const getPhotos = async (type: PhotoType): Promise<Photo[]> => {
  return await api.get(`/api/photos/${type}`) as Photo[]
}

const uploadPhoto = async (file: File, title: string, type: PhotoType): Promise<Photo> => {
  return await api.postFile('/api/photos/file', {title, type}, {file}) as Photo
}

const deletePhoto = async (id: string|undefined): Promise<Record<string, unknown>> => {
  return await api.delete(`/api/photos/${id}`)
}

export {getPhotos, uploadPhoto, deletePhoto}
