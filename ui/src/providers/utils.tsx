import React, {ReactElement, ReactNode} from 'react'

type ProviderType = ({children}: { children: ReactNode | ReactNode[] }) => ReactElement

export function withProvider<T>(Provider: ProviderType, Component: React.FC<T>) {
  return (props: T) => {
    return <Provider><Component {...props} /></Provider>
  }
}