import React, {ReactNode, useState} from 'react'

export const ModalContext = React.createContext({} as any)

const ModalProvider = ({children}: { children: ReactNode }) => {
  const [component, setComponent] = useState<ReactNode | null>(null)

  return (
    <ModalContext.Provider value={{component, setComponent}}>
      {children}
    </ModalContext.Provider>
  )
}

export default ModalProvider