import React, {ReactElement, useEffect, useState} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import {Photo, PhotoType} from 'models/Photo'
import {getPhotos} from '../repositories/photos'
import {ReactComponent as ShowPrev} from 'icons/caret-left.svg'
import {ReactComponent as ShowNext} from 'icons/caret-right.svg'
import ThumbnailBar from './ThumbnailBar'

interface RouteParams {
  id: string,
  type: PhotoType
}

const FullScreenPage: React.FC = (): ReactElement => {
  const {id, type} = useParams<RouteParams>()
  const history = useHistory()
  const [photos, setPhotos] = useState<Photo[]>([])
  const [photoIndex, setPhotoIndex] = useState(0)

  useEffect(() => {
    getPhotos(type).then(photos => {
      setPhotos(photos)
      toggleFullscreen()
    })

    document.addEventListener('fullscreenchange', onFullScreenChange)
    return () => {
      document.removeEventListener('fullscreenchange', onFullScreenChange)
    }
  }, [])

  useEffect(() => {
    setPhotoIndex(getIndex(id))

    document.addEventListener('keydown', onKeyDown)
    return () => {
      document.removeEventListener('keydown', onKeyDown)
    }
  }, [photos])

  const toggleFullscreen = () => {
    const elem = document.querySelector('.full-screen')

    elem && elem.requestFullscreen().catch(error => {
      if (error) return history.push('/photos/' + type)
    })
  }

  const onKeyDown = (event: KeyboardEvent) => {
    if (event.code === 'ArrowRight') showNext()
    if (event.code === 'ArrowLeft') showPrev()
  }

  const onFullScreenChange = () => {if (!document.fullscreenElement) history.push('/photos/' + type)}

  const getUrl = (index: number) => {
    if (getWidth() > 1920) return photos[index].url!.original
    else return photos[index].url!.fullHd
  }

  const getIndex = (photoId: string): number => photos.length ? photos.indexOf(photos.find(photo => photo._id === photoId)!) : 0

  const getWidth = () => window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth

  const showPrev = () => setPhotoIndex(photoIndex => Math.max(0, photoIndex - 1))

  const showNext = () => setPhotoIndex(photoIndex => Math.min(photoIndex + 1, photos.length - 1))

  const jumpTo = (index: number) => setPhotoIndex(index)

  return (
    <div className="full-screen d-flex justify-content-center">
      <img className="img-fluid main-image" src={photos.length && getUrl(photoIndex) || ''} alt=""/>
      {photoIndex + 1 > 1 && <div className="prev-image" onClick={showPrev}><ShowPrev/></div>}
      {photoIndex + 1 !== photos.length && <div className="next-image" onClick={showNext}><ShowNext/></div>}
      <div className="position-pointer">{`${photoIndex + 1} / ${photos.length}`}</div>
      <ThumbnailBar photos={photos} jumpTo={jumpTo} />
    </div>
  )
}

export default FullScreenPage