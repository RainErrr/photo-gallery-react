import React, {ReactElement, useEffect, useState} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import {Photo, PhotoType} from 'models/Photo'
import {ReactComponent as DeletePhoto} from 'icons/minus.svg'
import Toolbar from './Toolbar'
import useModal from '../hooks/useModal'
import {deletePhoto, getPhotos} from '../repositories/photos'
import PhotoForm from './PhotoForm'

interface PhotosPageProps {
  photos: Photo[],
  onDeletePhoto: (id: string) => void,
  showDeletePhotosToolbar: boolean,
}

interface RouteParams {
  type: PhotoType
}

const PhotosPage: React.FC<PhotosPageProps> = (): ReactElement => {
  const {type} = useParams<RouteParams>()
  const history = useHistory()
  const {showModal} = useModal()
  const [photos, setPhotos] = useState<Photo[]>([])
  const [showDeletePhotosToolbar, setShowDeletePhotosToolbar] = useState<boolean>(false)

  useEffect(() => {
    getPhotos(type).then(setPhotos)

    document.addEventListener('scroll', onScroll, false)
    return () => {
      document.removeEventListener('scroll', onScroll)
    }
  }, [])

  const onScroll = () => createObserver([...document.querySelectorAll('img.photo')])

  const createObserver = (htmlElements: Element[]) => {
    const options = {root: null, rootMargin: "0px"}
    const observer = new IntersectionObserver(handleIntersect, options)
    htmlElements.forEach(element => observer.observe(element))
  }

  const handleIntersect = (entries: IntersectionObserverEntry[], observer: IntersectionObserver) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        const img = (entry.target as HTMLImageElement)
        img.style.opacity = '1'
        img.style.transition = '.8s opacity'
        observer.unobserve(entry.target)
      }
    })
  }

  const displayFullScreen = (event: React.MouseEvent, photo: Photo) => {
    event.preventDefault()
    history.push(`/photos/${type}/${photo._id}`)
  }

  const layoutMap = () => {
    let result: Photo[][] = []
    for (let i = 0; i < 3; i++) result.push([])
    return photos.reduce((p, c, i) => {
      p[i%3].push(c)
      return p
    }, result)
  }

  const onAddPhoto = () => {
    setShowDeletePhotosToolbar(false)
    showModal(<PhotoForm onPhotoAdded={photo => setPhotos([...photos, photo])} type={type}/>)
  }

  const onDeletePhoto = async (id: string) => {
    try {
      await deletePhoto(id).then(() => setPhotos(photos.filter(photo => photo._id !== id)))
    } catch (error) {
      console.log(error)
    }
  }

  const showDeletePhotos = () => setShowDeletePhotosToolbar(!showDeletePhotosToolbar)
  return (
    <>
      <div className="toolbar toolbar-main">
        <Toolbar title={type === PhotoType.COLOR ? "World of colors" : "Black & white"} onAddPhoto={onAddPhoto} onShowDeletePhoto={showDeletePhotos} showDeletePhoto={showDeletePhotosToolbar}/>
      </div>
      <div className="masonry-container">
        <div className="masonry">
          {layoutMap().map((column, columnIndex) => {
            return (
              <div className="masonry-column" key={columnIndex}>
                {column.map((photo, photoIndex) => {
                  return (
                    <div key={photoIndex}>
                      {showDeletePhotosToolbar && (
                        <div className="d-flex justify-content-end">
                          <button className="btn delete-icon position-absolute d-flex justify-content-center align-items-center mt-3 me-3"
                                  onClick={() => onDeletePhoto(photo._id.toString())}>
                            <DeletePhoto/>
                          </button>
                        </div>
                      )}
                      <img src={photo.url.fullHd} alt="" style={{width: '100%', opacity: columnIndex <= 2 && photoIndex <= 1 ? '1' : '0'}} className="photo"
                           onClick={(event) => displayFullScreen(event, photo)}/>
                    </div>
                  )
                })}
              </div>
            )
          })}
        </div>
      </div>
    </>
  )
}

export default PhotosPage
