import React, {ReactElement, useState} from 'react'
import useModal from '../hooks/useModal'
import {uploadPhoto} from '../repositories/photos'
import {Photo, PhotoType} from 'models/Photo'

interface PhotoFormProps {
  onPhotoAdded: (photo: Photo) => void,
  type: PhotoType
}

const PhotoForm: React.FC<PhotoFormProps> = (props): ReactElement => {
  const {onPhotoAdded, type} = props
  const {closeModal} = useModal()
  const [photoTitle, setPhotoTitle] = useState<string>('')
  const [file, setFile] = useState<File| null>(null)
  const [loading, setLoading] = useState(false)

  const onTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    setPhotoTitle(() => (event.target.value))
  }

  const onFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target!.files![0]
    setFile(() => (file || null))
  }

  const onSubmit = (event: React.MouseEvent<HTMLButtonElement> | React.FormEvent) => {
    event.preventDefault()
    setLoading(true)
    if (!file) return
    uploadPhoto(file, photoTitle, type).then(photo => {
      onPhotoAdded(photo)
      setLoading(false)
      closeModal()
    }).catch(error => console.log(error))
  }

  const previewPhoto = () => file ? URL.createObjectURL(file) : ''

  return (
    <form>
      <div className="modal-body">
        <div className="input-group align-middle mb-2">
          <input placeholder="Insert title" className="form-control" type="text" value={photoTitle} onChange={onTitleChange}/>
        </div>
        <div className="input-group mt-3">
          <input type="file" className="form-control-file" onChange={onFileChange}/>
        </div>
      </div>
      <div className="d-flex justify-content-center">
        <img src={previewPhoto()} alt="" style={{maxHeight: '220px', width: 'auto'}}/>
      </div>
      <div className="modal-footer">
        <button className="btn btn-outline-secondary rounded float-right" disabled={loading || !file} onClick={onSubmit}>
          {loading ?
            <>
              <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"/> Loading...
            </>
            : 'Submit'}
        </button>
      </div>
    </form>
  )
}

export default PhotoForm