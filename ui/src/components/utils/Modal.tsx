import React, {ReactNode, useEffect} from 'react'
import {ReactComponent as CloseIcon} from 'icons/x-circle-bold.svg'

interface ModalProps {
  component: ReactNode,
  onClose: (e: React.MouseEvent | Event) => void
}

export enum EventType {CLOSE_MODAL= 'CLOSE_MODAL'}

const Modal: React.FC<ModalProps> = ({onClose, component}) => {
  useEffect(() => {
    document.addEventListener(EventType.CLOSE_MODAL, onClose)
    document.body.classList.add('overflow-visible', 'modal-open')
    document.addEventListener('keydown', onKeyDown)

    return () => {
      document.body.classList.remove('overflow-visible', 'modal-open')
      document.removeEventListener('keydown', onKeyDown)
      document.removeEventListener(EventType.CLOSE_MODAL, onClose)
    }
  }, [])

  const onKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Escape') {
      onClose(event)
    }
  }

  const onCloseModal = () => {
    document.dispatchEvent(new CustomEvent(EventType.CLOSE_MODAL))
  }

  return (
    <div id="modals-container">
      <div className="modal show">
        <div className="modal-dialog modal-dialog-centered modal-md">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Add photo</h5>
              <button type="button" className="btn-close d-flex align-items-center justify-content-center" onClick={onCloseModal}>
                <CloseIcon/>
              </button>
            </div>
            {component}
          </div>
        </div>
      </div>
      <div className="modal-backdrop fade show"/>
    </div>
  )
}

export default Modal