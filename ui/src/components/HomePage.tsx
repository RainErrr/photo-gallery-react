import React, {ReactElement} from 'react'
import {Link} from 'react-router-dom'
import {PhotoType} from 'models/Photo'

const HomePage: React.FC = (): ReactElement => {
  return (
    <div className="wrapper">
      <section className="section parallax bg1">
        <h1>Welcome</h1>
      </section>
      <section className="section static">
        <Link to={"/photos/" + PhotoType.BLACKANDWHITE} className="nav-link">
          <h1>"Welcome to Black & White"</h1>
        </Link>
      </section>
      <section className="section parallax bg2">
        <Link to={"/photos/" + PhotoType.COLOR} className="nav-link">
          <h1>"Welcome to the world of colors"</h1>
        </Link>
      </section>
      <section className="section static footer">
        <h1>Footer</h1>
      </section>
    </div>
  )
}

export default HomePage