import React, {ReactElement} from 'react'
import {Link} from 'react-router-dom'
import {ReactComponent as AddPhoto} from 'icons/plus.svg'
import {ReactComponent as ShowDeletePhoto} from 'icons/trash.svg'
import {ReactComponent as ScrollStart} from 'icons/caret-up.svg'
import {ReactComponent as ScrollEnd} from 'icons/caret-down.svg'
import {ReactComponent as BackHome} from 'icons/home.svg'

interface ToolBarProps {
  title: string,
  onAddPhoto: () => void,
  onShowDeletePhoto: () => void,
  showDeletePhoto: boolean
}

const Toolbar: React.FC<ToolBarProps> = (props): ReactElement => {
  const {title, onAddPhoto, onShowDeletePhoto, showDeletePhoto} = props

  const addPhoto = (event: React.MouseEvent) => {
    event.preventDefault()
    onAddPhoto()
  }

  const onClickShowDeletePhoto = (event: React.MouseEvent) => {
    event.preventDefault()
    onShowDeletePhoto()
  }

  const scrollToStart = () => window.scrollTo({top: 0, behavior: 'smooth'})

  const scrollToEnd = (event: React.MouseEvent) => {
    event.preventDefault()
    if (window.scrollY === 0) window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'})
  }

  return (
    <div className="container-fluid">
      <div className="row flex-grow-1">
        <div className="col-4 toolbar">
          <div className="h5 toolbar-brand">
            {title}
          </div>
        </div>
        <div className="col-4 d-flex justify-content-center">
          <div className="toolbar navbar-expand">
            <a className="nav-link" href="#" onClick={scrollToStart}>
              <ScrollStart/>
            </a>
            <Link to="/" className="nav-link">
              <BackHome/>
            </Link>
            <a className="nav-link" href="#" onClick={event => scrollToEnd(event)}>
              <ScrollEnd/>
            </a>
          </div>
        </div>
        <div className="col-4 d-flex justify-content-end">
          <div className="toolbar navbar-expand">
            <a className="nav-link" href="#" onClick={addPhoto}>
              <AddPhoto/>
            </a>
            <div className="nav-item">
              <a className={showDeletePhoto ? "nav-link active" : "nav-link"} href="#" onClick={onClickShowDeletePhoto}>
                <ShowDeletePhoto/>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Toolbar