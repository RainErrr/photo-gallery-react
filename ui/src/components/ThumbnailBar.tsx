import React, {ReactElement} from 'react'
import {Photo} from 'models/Photo'

interface ThumbnailBarProps {
  photos: Photo[],
  jumpTo: (index: number) => void
}

const ThumbnailBar: React.FC<ThumbnailBarProps> = (props): ReactElement => {
  const {photos, jumpTo} = props

  return (
    <div className="bottom-bar">
      <div className="scrollbar">
        {photos.map((photo, index) => {
          return (
            <img key={photo._id.toString()} src={photo.url.thumbnail} alt="" className="me-1"
                 onClick={() => jumpTo(index)}/>
          )
        })}
      </div>
    </div>
  )
}

export default ThumbnailBar