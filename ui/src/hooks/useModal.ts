import {ReactNode, useContext} from 'react'
import {ModalContext} from '../providers/ModalProvider'

interface ModalState {
  component: ReactNode
  showModal: (modal: ReactNode) => void
  closeModal: () => void
}

const useModal = (): ModalState => {
  const {component, setComponent} = useContext(ModalContext)

  const showModal = (modal: ReactNode) => {
    setComponent(modal)
  }

  const closeModal = () => {
    setComponent(null)
  }

  return {component, showModal, closeModal}
}

export default useModal