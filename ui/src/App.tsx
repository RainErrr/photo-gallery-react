import React, {ReactElement} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import './assets/scss/main.scss'
import PhotosPage from './components/PhotosPage'
import FullScreenPage from './components/FullScreenPage'
import Modal from './components/utils/Modal'
import useModal from './hooks/useModal'
import ModalProvider from './providers/ModalProvider'
import {withProvider} from './providers/utils'
import HomePage from './components/HomePage'

const App: React.FC = (): ReactElement => {
  const {component: modalComponent, closeModal} = useModal()
  return (
    <>
      <div>
        <Router>
          <Switch>
            <Route path={"/photos/:type/:id"} component={FullScreenPage}/>
            <Route path={"/photos/:type"} component={PhotosPage}/>
            <Route exact={true} path="/" component={HomePage}/>
          </Switch>
        </Router>
      </div>
      {modalComponent && <Modal component={modalComponent} onClose={closeModal}/>}
    </>
  )
}

export default withProvider(ModalProvider, App)
