import StatusCodes from 'http-status-codes'

const {NO_CONTENT} = StatusCodes

const defaultHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
}

class Api {
  async get(url: string) {
    return await fetch(url, {headers: defaultHeaders}).then(Api.parseResponse)
  }

  async postFile(url: string, data: Record<string, any>, files: { [key: string]: File }) {
    const formData = new FormData()

    for (const [key, file] of Object.entries(files)) {
      formData.append(key, file)
    }

    for (const [key, value] of Object.entries(data)) {
      formData.append(key, value)
    }

    return await fetch(url, {
      method: 'POST',
      body: formData,
    }).then(Api.parseResponse)
  }

  async delete(url: string, data: Record<string, unknown> = {}) {
    return await Api.request(url, data, 'DELETE')
  }

  private static async request(url: string, data: Record<string, unknown>, methodName: 'DELETE') {
    return await fetch(url, {
      method: methodName,
      headers: defaultHeaders,
      body: JSON.stringify(data)
    }).then(Api.parseResponse)
  }

  private static async parseResponse(response: Response): Promise<any> {
    const json = response.status === NO_CONTENT ? {} : await response.json()
    if (response.ok) {
      return json
    } else {
      return Promise.reject({status: response.status, ...json})
    }
  }
}

export default new Api()