import {Request, Response, Router} from 'express'
import PhotoService from '../services/PhotoService'
import {UploadedFile} from 'express-fileupload'
import {StatusCodes} from 'http-status-codes'
import {ObjectId} from 'bson'
import {PhotoType} from 'models/Photo'

interface PostFile {
  file: UploadedFile
  title: string
  type: PhotoType
}

const {BAD_REQUEST, NO_CONTENT} = StatusCodes

export const photoRoutes = (photoService: PhotoService): Router => {
  const router = Router()

  router.get('/:type', async (req: Request, res: Response) => {
    const {type} = req.params
    const photos = await photoService.all(type as PhotoType)
    return res.json(photos)
  })

  router.post('/file', async (req: Request, res: Response) => {
    try {
      const {file, title, type} = {...req.body, ...req.files} as PostFile

      const image = await photoService.createByFile(file, title, type)
      res.json(image)
    } catch (e) {
      res.status(BAD_REQUEST).end()
    }
  })

  router.delete('/:id', async (req: Request, res: Response) => {
    const {id} = req.params
    await photoService.delete(new ObjectId(id))
    return res.status(NO_CONTENT).end()
  })

  return router
}
