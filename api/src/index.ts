import {createServer} from './Server'

const port = process.env.NODE_ENV === 'development' ? 4000 : parseInt(process.env.PORT!!)

createServer().then(app => {
  app.listen(port, () => {
    console.info(`Express server started on port: ${port}`)
  })
}).catch(console.error)

