import Repository from '../repositories/Repository'
import {ObjectId} from 'bson'
import {UploadedFile} from 'express-fileupload'
import LocalFileService from './LocalFileService'
import S3FileService from './S3FileService'
import sharp from 'sharp'
import {ServiceError} from '../exceptions'
import {Photo, PhotoType, Url} from 'models/Photo'

class PhotoService {
  constructor(private fileService: S3FileService | LocalFileService, private photoRepository: Repository<Photo>) {}

  async all(type: PhotoType) {
    return await this.photoRepository.allBy(type)
  }

  async createByFile(file: UploadedFile, title: string, type: PhotoType): Promise<Photo> {
    return await this.createPhoto(file.data, file.name, title, type)
  }

  async delete(id: ObjectId) {
    const photo = await this.photoRepository.find(id)

    if (!photo) throw new ServiceError('Photo not found', {id: 'invalid'})

    await this.fileService.delete(id)
    await this.photoRepository.delete(id)
  }

  private async createPhoto(data: Buffer, filename: string, title: string, type: PhotoType) {
    const _id: ObjectId = new ObjectId()
    const url = await this.resizeAndUpload(_id, data, filename)
    const photo = {url, _id, title, type}

    return await this.photoRepository.create(photo)
  }

  async resizeAndUpload(_id: ObjectId, data: Buffer, filename: string): Promise<Url> {
    const dimensions = [
      {name: 'original', width: 0, height: 0},
      {name: 'fullHd', width: 1920, height: 1080},
      {name: 'thumbnail', width: 320, height: 200}]

    const url: Url = {original: '', fullHd: '', thumbnail: ''}

    for (const dimension of dimensions) {
      const path = `${_id}/${dimension.name + '_'}${filename}`
      ;(url as any)[dimension.name] = `${process.env.ASSET_HOST}/${path}`
      const resizedPhoto = dimension.name === 'original' ?
        data :
        await sharp(data).rotate().resize(dimension.width, dimension.height, {fit: sharp.fit.inside}).toBuffer()

      await this.fileService.upload(resizedPhoto, path)
    }

    return url
  }
}

export default PhotoService
