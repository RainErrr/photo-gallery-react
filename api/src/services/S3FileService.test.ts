import S3FileService from './S3FileService'
import {mockEnv, restoreEnv} from '../../test/helpers'
import {ObjectId} from 'bson'

const s3client = {
  upload: jest.fn(),
  delete: jest.fn(),
  listObjectsV2: jest.fn(),
  deleteObjects: jest.fn(),
} as any

const bucketName = 'aws-bucket'
beforeEach(() => mockEnv({AWS_BUCKET: bucketName}))
afterEach(() => restoreEnv())

const fileService = new S3FileService(s3client)

describe('upload', () => {
  it('uploads file to S3', () => {
    const filename = 'filename'
    const data = Buffer.from('file content')
    const publicRead = 'public-read'
    s3client.upload.mockReturnValue({promise: jest.fn()})

    fileService.upload(data, filename).catch(error => console.log(error))

    const expected = {
      Bucket: bucketName,
      Key: filename,
      Body: data,
      ACL: publicRead,
    }

    expect(s3client.upload).toHaveBeenCalledWith(expected)
  })
})

describe('delete', () => {
  it('deletes a folder in s3', async () => {
    const id = new ObjectId()
    s3client.listObjectsV2
      .mockReturnValue({promise: jest.fn().mockResolvedValue({Contents: [{Key: 'listObject'}], IsTruncated: false})})
    s3client.deleteObjects
      .mockReturnValue({promise: jest.fn().mockResolvedValue({Deleted: [{Key: 'listObject'}]})})

    const actual = await fileService.delete(id).catch(error => console.log(error))

    const listParams = {
      Bucket: bucketName,
      Prefix: id.toString(),
    }
    expect(s3client.listObjectsV2).toHaveBeenCalledWith(listParams)

    const deleteParams = {
      Bucket: bucketName,
      Delete: {
        Objects: [{Key: 'listObject'}]
      }
    }
    expect(await s3client.deleteObjects).toHaveBeenCalledWith(deleteParams)
    expect(actual).toBe(1)
  })
})
