import Aws from 'aws-sdk'
import {ManagedUpload} from 'aws-sdk/lib/s3/managed_upload'
import {ObjectId} from 'bson'

Aws.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_ACCESS,
  region: process.env.AWS_REGION,
})

export default class S3FileService {
  constructor(private s3client = new Aws.S3()) {
  }

  async upload(data: Buffer, filePath: string): Promise<ManagedUpload.SendData> {
    const params = {
      Bucket: process.env.AWS_BUCKET as string,
      Key: filePath,
      Body: data,
      ACL: 'public-read',
    }

    return await this.s3client.upload(params).promise()
  }

  async delete(id: ObjectId) {
    let count = 0
    while (true) {
      const listParams = {
        Bucket: process.env.AWS_BUCKET as string,
        Prefix: id.toString(),
      }

      const listedObjects = await this.s3client.listObjectsV2(listParams).promise()

      if (listedObjects.Contents === undefined) {
        throw new Error('Listing S3 returns no contents')
      }

      if (listedObjects.Contents.length !== 0) {
        const deleteParams = {
          Bucket: process.env.AWS_BUCKET as string,
          Delete: {
            Objects: listedObjects.Contents.map(obj => ({
              Key: obj.Key as string,
            })),
          },
        }

        const result = await this.s3client.deleteObjects(deleteParams).promise()

        count += (result.Deleted as Aws.S3.DeletedObjects).length
      }

      if (!listedObjects.IsTruncated) return count
    }
  }
}