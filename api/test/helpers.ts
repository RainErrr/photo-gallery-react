import express, {Express} from 'express'
import 'express-async-errors'
import errorHandler from '../src/server/prepare/errorHandler'
import middleware from '../src/server/prepare/middleware'
import mockedEnv from 'mocked-env'

let _restoreEnv: any | undefined

export const mockEnv = (env: any) => {
  _restoreEnv = mockedEnv(env)
}

export const restoreEnv = () => {
  if (_restoreEnv) {
    _restoreEnv()
  }

  _restoreEnv = undefined
}

export const createTestApp = (setup: (server: Express) => Express) => {
  const server = express()

  server.use(express.static('public'))
  middleware(server)
  setup(server)
  server.use(errorHandler)

  return server
}
